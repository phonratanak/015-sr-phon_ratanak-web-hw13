import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './component/header';
import Contain from './component/contain';
import {Container,Spinner} from 'react-bootstrap';
import axios from 'axios';
import './App.css';
import AddArticle from './component/addArticle';
import UpdateArticle from './component/updateArticle';
import Detail from './component/detail';
import {BrowserRouter as Router,Switch,Route} from "react-router-dom";
export default class App extends Component {
  constructor(props)
  {
    super(props);
    this.state={
      person:[],
      loading:true
    }
  }
  refresh=()=>
  {
    axios.get("http://110.74.194.124:15011/v1/api/articles?page=1&limit=15")
    .then((res)=>{
      this.setState(
        {
          person:res.data.DATA,loading:false
        }
      )
    })
  }
  componentWillMount()
  {
    this.refresh();
  }
  render() {
    return (
      <div>
        <Container>
          <Router>
            <Header/>
            <Switch>
              {this.state.loading?
              <div style={{padding:'50px 0px'}} className="App">
                <Spinner animation="border" style={{padding:'20px'}} />
              </div> 
               :
              <Route  exact path='/' render={(props)=><Contain {...props} data={this.state.person} refresh={this.refresh}></Contain>}/>
              }
              <Route path="/AddArticle"  render={(props)=><AddArticle {...props} refresh={this.refresh}></AddArticle>}/>
              <Route path="/UpdateArticle:id"  render={(props)=><UpdateArticle {...props} refresh={this.refresh}></UpdateArticle>}/>
              <Route path='/view:id' render={(props)=><Detail {...props} ></Detail>}></Route>
            </Switch>
          </Router>
        </Container>
      </div>
    )
  }
}
