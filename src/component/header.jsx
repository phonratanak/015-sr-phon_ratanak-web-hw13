import React from 'react'
import { Button,Navbar,Nav,Form,FormControl } from 'react-bootstrap';
import {Link} from "react-router-dom";
export default function header() {
    return (
        <div>
            <Navbar bg="light" variant="light">
                <Navbar.Brand >AMS</Navbar.Brand>
                <Nav className="mr-auto">
                <Nav.Link as={Link} to="/">Home</Nav.Link>
                </Nav>
                <Form inline>
                <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                <Button variant="outline-primary">Search</Button>
                </Form>
            </Navbar>
        </div>
    )
}
