import React, { Component } from 'react'
import { Button,Form,Container,Row,Col,Card } from 'react-bootstrap';
import axios from 'axios';
export default class addContext extends Component {
    constructor(props)
    {
        super(props);
        this.state={
            title:'',
            description:'',
            validateTitle:'',
            validateDes:''
        }
    }
    validation=()=>
    {
        let validateTitle,validateDes;
        if(!this.state.title)
        {
            validateTitle="value is empty";
        }
        if(!this.state.description)
        {
            validateDes="value is empty";
        }
        if(validateTitle || validateDes)
        {
            this.setState({validateTitle,validateDes})
            return false;
        }
        return true;
    }
    addValue=(event)=> {
        event.preventDefault();
        // eslint-disable-next-line no-unused-vars
        let validate=this.validation();
        if(validate){
            let article={
                TITLE:this.state.title,
                DESCRIPTION:this.state.description,
                IMAGE:'https://www.crowdforapps.com/assets/uploads/files/reactjs-features-250919.png',
            }
            axios.post("http://110.74.194.124:15011/v1/api/articles",article)
            .then((res)=>{alert(res.data.MESSAGE);this.props.history.push('/');this.props.refresh()})
        }

    }
    handleChange=(event)=>
    {
        let nam = event.target.name;
        let val = event.target.value;
        this.setState({[nam]: val});
    }
    render() {
        return (
            <div style={{marginTop:'50px'}}>
            <Container>
                <Row>
                    <Col md="8">
                        <Form>
                            <Form.Group controlId="formBasicEmail">
                                <Form.Label>TITLE</Form.Label>
                                <Form.Control name="title" value={this.state.title} type="text" placeholder="enter title" onChange={this.handleChange}/>
                                <span style={{color:'red'}}>{this.state.validateTitle}</span>
                            </Form.Group>
                            <Form.Group controlId="formBasicPassword">
                                <Form.Label>DESCRIPTION</Form.Label>
                                <Form.Control name="description" type="text" placeholder="enter description" value={this.state.description} onChange={this.handleChange} />
                                <span style={{color:'red'}}>{this.state.validateDes}</span>
                            </Form.Group>
                            <Button variant="primary" type="submit" onClick={this.addValue}>
                                ADD
                            </Button>
                        </Form>
                    </Col>
                    <Col md="4">
                    <Card >
                    <Card.Img variant="top" src="https://www.eltis.org/sites/default/files/default_images/photo_default_4.png?fbclid=IwAR14FUJLjdX7gCSrkxzZi8q7IblLvVTAzt73Zgv9-eMwfRmbsf6W4SinoWo" />
                    </Card>
                    </Col>
                </Row>
            </Container>

            </div>
        )
    }
}
