import React from 'react'
import { Table} from 'react-bootstrap';
import { Button} from 'react-bootstrap';
import {Link} from "react-router-dom";
import axios from 'axios';
export default function contain(props) {
    
    let value=props.data.map((val)=>
    (
        <tr key={val.ID}>
        <td>{val.ID}</td>
        <td>{val.TITLE}</td>
        <td>{val.DESCRIPTION}</td>
        <td>{val.CREATED_DATE}</td>
        <td><img style={{width:"100px"}} src={val.IMAGE}></img></td>
        <td className="tableBtn">
                <Link to={`/View${val.ID}`}>
                    <Button variant="primary">View</Button>
                </Link>
                <Link to={`/UpdateArticle${val.ID}`}>
                    <Button variant="secondary">Edit</Button>
                </Link>
                <Button variant="success" onClick={()=>axios.delete(`http://110.74.194.124:15011/v1/api/articles/${val.ID}`)
                                                    .then((res)=>{alert(res.data.MESSAGE);props.refresh()})}
                >Delete</Button>
        </td>
        </tr>
    )
    );
    return (
        <div style={{textAlign:'center'}}>
            <h1>Article Management</h1>
            <Link to="/addArticle">
                <Button variant="secondary">Add New Article</Button>
            </Link>
            <Table striped bordered hover size="sm" style={{marginTop:'20px'}}>
                <thead>
                    <tr>
                    <th>#</th>
                    <th>TITLE</th>
                    <th>DESCRIPTION</th>
                    <th>CREATE DATE</th>
                    <th>IMAGE</th>
                    <th>ACTION</th>
                    </tr>
                </thead>
                <tbody>
                    {value}
                </tbody>
            </Table>
        </div>
    )
}
