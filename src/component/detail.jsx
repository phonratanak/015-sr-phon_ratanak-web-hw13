
import {Container,Row,Col,Button,Spinner} from 'react-bootstrap';
import axios from 'axios';
import React, { Component } from 'react'

export default class detail extends Component {
    constructor(props)
    {
        super(props);
        this.state={
            data:[],
        }
    }
    componentDidMount()
    {
        axios.get(`http://110.74.194.124:15011/v1/api/articles/${this.props.match.params.id}`)
        .then((res)=>{this.setState({data:res.data.DATA})})
    }
    render() {
        return (
            <div>
                <Container style={{marginTop:'20px'}}>
                    <Row>
                        <Col md="2" className="img-contain">
                            <img  src={this.state.data.IMAGE}></img>
                        </Col>
                        <Col md="10">
                            <h4>{this.state.data.TITLE}</h4><br/>
                            <p>{this.state.data.DESCRIPTION}</p>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }
}



